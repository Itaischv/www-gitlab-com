---
layout: markdown_page
title: "Category Vision - Continuous Integration"
---

- TOC
{:toc}

## Continuous Integration

Many teams report that CI, while important and commonly adopted, is too slow and teams are unsure how to speed it up. It's often a black box that few people understand and diving in feels like an interruption to the more important work of delivering features. This presents a unique opportunity for CI solutions that make optimization of the pipeline easier and more automatic.

While we are very proud that GitLab CI/CD was recognized as [the leading CI/CD tool on the market](/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), and has been called `lovable` by many, many users - we still have a vision for how we can make what we've built even better.  In other epics, we talk about [Speed & Scalability](https://gitlab.com/groups/gitlab-org/-/epics/786) and [making CI lovable for every use case](https://gitlab.com/groups/gitlab-org/-/epics/811).  This vision epic will cover the broadest area of CI in GitLab - and how we're going to go from `lovable` to `undeniably amazing`. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3AContinuous%20Integration)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1304) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### Continuous Integration vs. Delivery vs. Deployment

CI and CD are very closely related areas at GitLab: our CI platform (what we call Verify) provides the foundation for CD and is the [Gateway to Operations](/direction/verify/#gateway-to-operations).  In fact, Verify is also the first stage in our [CI/CD section](/handbook/product/categories/#cicd-section) which also includes Package and Release.  In addition to the vision for each of those stages, we maintain a section wide [vision for CI/CD](/direction/cicd/)

### GitLab Runner

The GitLab Runner is managed by the Verify team also, and is a key component of the CI solution. You can find the vision epic for the Runner at [gitlab-org#573](https://gitlab.com/groups/gitlab-org/-/epics/573).

## What's Next & Why

Up next are a few MVC epics and issues to help advance our 2019 Product Vision:

* Vault integration as part of [gitlab-org#816](https://gitlab.com/groups/gitlab-org/-/epics/816)
* [Directed acyclic graphs (DAG) for pipelines MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063)

The DAG opens up new possibilities for building pipelines within GitLab, including for monorepos or data science, and Vault integration adds to the security of GitLab CI pipelines.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our 
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. It's important to us that
we defend our lovable status, though, so if you see any actual or potential gap please let us know in
our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1307) for this category.

## Competitive Landscape

In general, to be more competitive we are considering [gitlab-ee#10107](https://gitlab.com/gitlab-org/gitlab-ee/issues/10107) to move GitLab CI/CD for External Repos (GitHub + BitBucket) from Premium to Starter. This will help more teams have access to GitLab CI/CD, and is potentially a great way to introduce more people to GitLab in general, but also comes with costs of running the CI/CD so the decision needs to be weighed closely against the opportunity.

### Azure DevOps/GitHub

Microsoft is planning to use their GitHub acquisition to pick up market share in going after open source software development for their new integrated solution. There had been a trend to move away from them because users saw their current development tools as old-fashioned; with rebranding them to Azure Devops (and investment in improving their capabilities) they are trying to disrupt this decision by their customers. We need to ensure that our support for Microsoft stacks is high quality, and issues like [gitlab-ce#61970](https://gitlab.com/gitlab-org/gitlab-ce/issues/61970) will help us achieve that by making sure the experience using GitLab for .NET development is just as good as on Azure DevOps.

Additionally, GitHub actions has introduced a new kind of system for CI that offers a marketplace of snippets that can be contributed, linked together in easy ways including to systems traditionally outside of the scope of CI, and build pipelines for delivery. It's an interesting use case for straightforward use cases, and aligns to the decentralized model that software development is moving towards, but also could end up being quite complex and very hard to predict exactly what the system will do after everything is wired together.

To compete against GitHub Actions, we are looking at introducing directed acyclic graphs in the pipeline via [gitlab-ce#47063](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063), which will allow better performance within the pipeline by managing the dependencies directly instead of grouping things per stage. This is a marketable improvement that can be used to demonstrate parity with GitHub Actions.

Microsoft has also provided a [feature comparison vs. GitLab](https://docs.microsoft.com/en-us/azure/devops/learn/compare/azure-devops-vs-gitlab) which, although it is not 100% accurate, highlights the areas where they see an advantage over us. Areas noted are:

#### Triggers and Gates

We're in the middle of making a major overhaul to our cross-pipeline triggering capabilities. We already have a `trigger` keyword to start a downstream pipeline in any project, and we're introducing a `needs` keyword to define these relationships from the downstream itself via [gitlab-ee#9045](https://gitlab.com/gitlab-org/gitlab-ee/issues/9045). We're also adding status checking behaviors via [gitlab-ee#11238](https://gitlab.com/gitlab-org/gitlab-ee/issues/11238) that will enable control over waiting/acting on different 

As far as gates, we think it's important for CI/CD pipelines not to be built with manual approval gates in them. That said, we of course recognize the importance of collecting approvals as part of a well run, compliant delivery process. To that end we're implementing [gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187) to wait for approvals on the MR before proceeding, which can follow your standards for MR approvals. This bundles the approval nicely right into the natural development workflow, avoding a "process trap" where a sub-optimal solution is easy to gravitate towards and get stuck in. More information on how we're thinking about compliance in the release process can be viewed at our [Release Governance](/direction/release/release_governance) category page.

Build triggering and batching is also something we're looking into. We're introducing Merge Trains via [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186), with a quick follow up to add a parallelization strategy [gitlab-ee#11222](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222). Merge trains are a very powerful way to control the flow of changes into a target branch/environment by ensuring that master is always green. In concert with features like [pipelines on merge results](https://docs.gitlab.com/ee/ci/merge_request_pipelines/#pipelines-for-merged-results-premium), which run branch pipelines on the potential merge result into the target branch, it's very easy to keep a green master using GitLab.

#### Platforms

The feature comparison notes that we do not provide "native" support to deploy to AWS, Azure, or GCP, but we see it as a strength of our platform that we don't have a single preferred solution that we drive our users to. Auto DevOps works with Kubernetes on any of these platforms, including your own, and looks and acts identically to your developers. We do want to make it absolutely as easy as possible to deploy to these providers, even if you're not using Auto DevOps, so we have issues like [gitlab-ce#57780](https://gitlab.com/gitlab-org/gitlab-ce/issues/57780) which will provide syntactic sugar to make things simpler without locking you in.

They also list that we do not support ARM development, however this is false. We do have an open issue for running a GitLab Runner on ARM at [gitlab-runner#2076](https://gitlab.com/gitlab-org/gitlab-runner/issues/2076) which will allow you also to build _from_ ARM devices.

#### Orchestration

In the orchestration category it's listed that we don't provide a visual editor for build and release orchestration. This is true, but it's because we've prioritized trying to keep our YAML syntax clean and understandable. We have two related open issues: [gitlab-ce#20785](https://gitlab.com/gitlab-org/gitlab-ce/issues/20785) (visualize YAML) and [gitlab-ce#21485](https://gitlab.com/gitlab-org/gitlab-ce/issues/21485) (edit YAML) but for now we continue to plan to focus on keeping pipelines easy and efficient to work with as-code.

That said, there is a role for non-technical users to engage with pipelines - we just don't see that as the same thing as providing visual tools for editing release automation. For us, we are focused on solving these problems in the [Release Orchestration](/direction/release/release_orchestration) category where we want to provide better ways for these users to engage with the release process without worrying about (even abstracted) technical implementation details.

#### Code Quality & Security

Although the page says we do not, we support JUnit and Java testing natively via our support for [JUnit test reports](https://docs.gitlab.com/ee/ci/junit_test_reports.html). We also support a `parallel` [keyword](https://docs.gitlab.com/ee/ci/yaml/#parallel) for automatically parallelizing your test runs. We support .NET TRX format by leveraging [trx2junit](https://github.com/gfoidl/trx2junit), an open source conversion tool, and also plan to support the TRX format directly via [gitlab-ce#61970](https://gitlab.com/gitlab-org/gitlab-ce/issues/61970). We have plans to improve how we show test results over time via [gitlab-ee#1020](https://gitlab.com/gitlab-org/gitlab-ee/issues/1020). We also do actually already provide easy dynamic provisioning of environment in not just Azure or AWS, but any platform (including on-premise) using our [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

Mobile app testing is a priority for us, and you can read more at our [use case page for mobile](/direction/mobile). 

Finally, we do not rely on third-party plugins for security scanning (although you're welcome to bring your own). GitLab has this built in, and you can see our vision for how we are continuing to improve on these capabilities in our [Secure](/direction/secure/) and [Defend](/direction/defend/) category pages.

#### Deployment Targets

As opposed to the claim in the document, it's actually possible to run your own GitLab runners on premise without hosting your own GitLab instance. Simply register your runners and they will work without requiring opening up an inbound firewall port from GitLab. This works for any installation - on-premise Kubernetes, cloud, physical hardware - you can always keep your runners close to your code for security, and we support this out of the box.

Although the page refers to stale documentation for publishing to the App Store, we do have a recent blog post describing [how to get up and running with GitLab and FastLane](https://about.gitlab.com/2019/03/06/ios-publishing-with-gitlab-and-fastlane/) which has everything you need to get up and running doing iOS builds and App Store deployments with GitLab. More information can be found about our mobile strategy on our [Mobile Use Case](/direction/mobile) page.

#### Integrations and Extensions

GitLab CI/CD does in fact support [using an external repo](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/), although we don't support SVN or other non-git repos except through technologies like [SubGit](https://subgit.com/) which could provide a transparent gateway. We also can provide CI/CD for [GitHub](https://about.gitlab.com/solutions/github/) or [BitBucket](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/bitbucket_integration.html) repositories easily.

It's true we do not provide a marketplace for CI/CD plugins. For the moment, our strategy is that moving towards having GitLab rely on too many third party plugins is a major risk for DevOps teams, as any maintainer of a Jenkins server will testify. Instead, our strategy is to [play well with others](https://about.gitlab.com/handbook/product/#plays-well-with-others) and welcome anyone who wants to make integrations work together with GitLab. We just don't foresee, at least for now, a future where there's an ecosystem of third-party plugins that you need to draw from in order to get what would otherwise be basic functionality out of GitLab. If you see a gap here and would like some technology you use to work better with GitLab we'd love for you to create an issue and let us know.

### CloudBees Jenkins/CodeShip

Jenkins has been for a while the punching bag for how you sell DevOps software (just saying "my product solves the same problems as Jenkins, but without the huge expensive legacy mess" have given you the keys to the kingdom.) Jenkins is trying to address this by [splitting](https://jenkins.io/blog/2018/08/31/shifting-gears/) their product into multiple offerings, giving them the freedom to shed some technical and product debt and try to innovate again. They also acquired CodeShip, a SaaS solution for CI/CD so that they can play in the SaaS space. All of this creates a complicated message that doesn't seem to be resonating with analysts or customers yet.

At the moment there isn't a lot that needs to be done here to maintain our lead over the product, but continuing to add product depth (that does not add complexity) via features like [gitlab-ce#`17081`](https://gitlab.com/gitlab-org/gitlab-ce/issues/17081) will allow us to clearly demonstrate that our product is as mature as Jenkins but without the headache.

### Bitbucket Pipelines and Pipes
BitBucket pipelines has been Atlassian's answer to a more CI/CD approach than the traditional Atlassian Bamboo that is very UI driven builds and deploys by allowing users to create yml based builds.

On February 28, 2019, Atlassian announced [Bitbucket Pipes](https://bitbucket.org/blog/meet-bitbucket-pipes-30-ways-to-automate-your-ci-cd-pipeline) as an evolution of Bitbucket pipelines and gained a lot of press around [Atlassian taking on GitHub/Microsoft](https://www.businessinsider.com/atlassian-bitbucket-pipes-ci-cd-gitlab-github-2019-2).  While it is early in the evolution of this as a product in the enterprise market, there are a number of interesting patterns in the announcement that favor Convention over Configuration.  The feature that most closely matches this in our roadmap is [gitlab-ce#53307](https://gitlab.com/gitlab-org/gitlab-ce/issues/53307).

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution:

- GitLab is seen as the best end to end leader where other products are not keeping up and not providing a comprehensive solution. We should continue to build a deep solution here in order to stay ahead of competitor's solutions.
- Competitors claim that GitLab can go down and doesn't scale, which is perhaps their best argument if their products do not provide as comprehensive solutions. This isn't really true, so we need to have messaging and product features that make this clear.
- In general, cloud adoption of CI/CD is growing and our CI needs to be ready.

To continue to drive in this area, we are considering [gitlab-ce#40720](https://gitlab.com/gitlab-org/gitlab-ce/issues/40720) next to add Vault integration throughout the CI pipeline. This builds off of work happening on the Configure team and will allow for a more mature delivery approach that takes advantage of ephemeral credentials to ensure a rock solid secure pipeline.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers were:

* CI Views - specifically for [JUnit XML results](https://gitlab.com/gitlab-org/gitlab-ce/issues/17081)
* [`include:` keyword not expanding variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/52972)
* [Filter pipelines by status or branch](https://gitlab.com/gitlab-org/gitlab-ce/issues/31690)
* [Requiring pipeline to succeed before merging MR no longer works with external CI](https://gitlab.com/gitlab-org/gitlab-ee/issues/6870)

## Top Customer Issue(s)

The most popular customer facing issue is integration with [Hashicorp Vault](https://www.vaultproject.io/) as part of [gitlab-org#816](https://gitlab.com/groups/gitlab-org/-/epics/816)

## Top Internal Customer Issue(s)

The issue about `triggered-by` functionality is currently the top internal customer issue: [gitlab-ee#9045](https://gitlab.com/gitlab-org/gitlab-ee/issues/9045).  Other top internal customer issues include:

* Showing a high visiblity alert if master is red [gitlab-ee#10216](https://gitlab.com/gitlab-org/gitlab-ee/issues/10216)
* Visualizing job duration [gitlab-ee#2666](https://gitlab.com/gitlab-org/gitlab-ee/issues/2666)

## Top Vision Item(s)

Our most important vision item is [gitlab-org#414](https://gitlab.com/groups/gitlab-org/-/epics/414) to allow first-class support for all cross-project triggering workflows, followed closely by Directed acyclic graphs (DAG) for pipelines MVC [gitlab-ce#47063](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063).

Additionally, we want to provide better support for our .NET users. Adding support for unit testing frameworks commonly used in those environments will help us become a better solution for everyone. [gitlab-ce#61970](https://gitlab.com/gitlab-org/gitlab-ce/issues/61970) introduces direct support for the TRX file format, avoiding the need for Microsoft shops to have to convert to JUnit.

Another item that is becoming more important is our use of [Docker Machine](https://github.com/docker/machine) for multi-cloud provisioning/autoscaling, which is now in maintenance mode. [gitlab-runner#4338](https://gitlab.com/gitlab-org/gitlab-runner/issues/4338) is where we're discussing what future options would look like.
